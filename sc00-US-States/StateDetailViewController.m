//
//  StateDetailViewController.m
//  sc00-US-States
//
//  Created by Orlando Gotera on 11/1/17.
//  Copyright © 2017 Orlando Gotera. All rights reserved.
//

#import "StateDetailViewController.h"
#import "StateInfo.h"

@interface StateDetailViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *stateFlagImage;
@property (weak, nonatomic) IBOutlet UILabel *detailStateName;
@property (weak, nonatomic) IBOutlet UILabel *detailStateCapital;
@property (weak, nonatomic) IBOutlet UILabel *stateDetailMotto;
@property (weak, nonatomic) IBOutlet UILabel *statePopulation;
@property (weak, nonatomic) IBOutlet UILabel *birdName;
@property (weak, nonatomic) IBOutlet UIImageView *birdImage;
@end

@implementation StateDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.stateFlagImage.image = self.myState.flag;
    self.detailStateName.text = self.myState.name;
    self.detailStateCapital.text = self.myState.capital;
    self.stateDetailMotto.text = self.myState.motto;
    self.statePopulation.text = self.myState.population;
    self.birdName.text = self.myState.birdName;
    self.birdImage.image = self.myState.bird;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
