//
//  StatesTableViewCell.h
//  sc00-US-States
//
//  Created by Orlando Gotera on 11/1/17.
//  Copyright © 2017 Orlando Gotera. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StatesTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *stateName;
@property (weak, nonatomic) IBOutlet UILabel *stateMotto;
@property (weak, nonatomic) IBOutlet UIImageView *flagImageView;

@end
