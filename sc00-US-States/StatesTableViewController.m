//
//  StatesTableViewController.m
//  sc00-US-States
//
//  Created by Orlando Gotera on 11/1/17.
//  Copyright © 2017 Orlando Gotera. All rights reserved.
//

#import "StatesTableViewController.h"
#import "StateInfo.h"
#import "StatesTableViewCell.h"
#import "StateDetailViewController.h"
@interface StatesTableViewController ()

@property(strong, nonatomic) NSMutableArray *usStates;

@end

@implementation StatesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    StateInfo *alabama = [[StateInfo alloc]init];
    alabama.name = @"Alabama";
    alabama.capital = @"Montgomery";
    alabama.motto = @"Audemus jura nostra defendere";
    alabama.flag = [UIImage imageNamed:@"Flags/Small/alabama-flag-small"];
    alabama.population = @"4,858,979";
    alabama.birdName = @"Northern flicker";
    alabama.bird = [UIImage imageNamed:@"StateBirds/alabamaBird.jpg"];
    
    StateInfo *alaska = [[StateInfo alloc]init];
    alaska.name = @"Alaska";
    alaska.capital = @"Juneau";
    alaska.motto = @"North to the Future";
    alaska.flag = [UIImage imageNamed:@"Flags/Small/alaska-flag-small"];
    alaska.population = @"738,432";
    alaska.birdName = @"Willow ptarmigan";
    alaska.bird = [UIImage imageNamed:@"StateBirds/alaskaBird.jpg"];
    
    StateInfo *arizona = [[StateInfo alloc]init];
    arizona.name = @"Arizona";
    arizona.capital = @"Phoenix";
    arizona.motto = @"Ditat Deus";
    arizona.flag = [UIImage imageNamed:@"Flags/Small/arizona-flag-small"];
    arizona.population = @"6,828,065";
    arizona.birdName = @"Cactus wren";
    arizona.bird = [UIImage imageNamed:@"StateBirds/arizonaBird.jpg"];
    
    StateInfo *arkansas = [[StateInfo alloc]init];
    arkansas.name = @"Arkansas";
    arkansas.capital = @"Little Rock";
    arkansas.motto = @"Regnat populus";
    arkansas.flag = [UIImage imageNamed:@"Flags/Small/arkansas-flag-small"];
    arkansas.population = @"2,978,204";
    arkansas.birdName = @"Northern mockingbird";
    arkansas.bird = [UIImage imageNamed:@"StateBirds/arkansasBird.jpg"];
    
    StateInfo *california = [[StateInfo alloc]init];
    california.name = @"California";
    california.capital = @"Sacramento";
    california.motto = @"Eureka";
    california.flag = [UIImage imageNamed:@"Flags/Small/california-flag-small"];
    california.population = @"39,144,818";
    california.birdName = @"California quail";
    california.bird = [UIImage imageNamed:@"StateBirds/californiaBird.jpg"];
    
    StateInfo *colorado = [[StateInfo alloc]init];
    colorado.name = @"Colorado";
    colorado.capital = @"Denver";
    colorado.motto = @"Nil sine Numine";
    colorado.flag = [UIImage imageNamed:@"Flags/Small/colorado-flag-small"];
    colorado.population = @"5,456,574";
    colorado.birdName = @"Lark bunting";
    colorado.bird = [UIImage imageNamed:@"StateBirds/coloradoBird.jpg"];
    
    StateInfo *connecticut = [[StateInfo alloc]init];
    connecticut.name = @"Connecticut";
    connecticut.capital = @"Hartford";
    connecticut.motto = @"Qui transtulit sustinet";
    connecticut.flag = [UIImage imageNamed:@"Flags/Small/connecticut-flag-small"];
    connecticut.population = @"3,590,886";
    connecticut.birdName = @"American robin";
    connecticut.bird = [UIImage imageNamed:@"StateBirds/connecticutBird.jpg"];
    
    StateInfo *florida = [[StateInfo alloc]init];
    florida.name = @"Florida";
    florida.capital = @"Tallahasse";
    florida.motto = @"In God We Trust";
    florida.flag = [UIImage imageNamed:@"Flags/Small/florida-flag-small"];
    florida.population = @"20,271,272";
    florida.birdName = @"Northern mockingbird";
    florida.bird = [UIImage imageNamed:@"StateBirds/floridaBird.jpg"];
    
    StateInfo *georgia = [[StateInfo alloc]init];
    georgia.name = @"Georgia";
    georgia.capital = @"Atlanta";
    georgia.motto = @"Wisdom, justice, and moderation";
    georgia.flag = [UIImage imageNamed:@"Flags/Small/georgia-flag-small"];
    georgia.population = @"10,214,860";
    georgia.birdName = @"Brown thrasher";
    georgia.bird = [UIImage imageNamed:@"StateBirds/georgiaBird.jpg"];
    
    StateInfo *hawaii = [[StateInfo alloc]init];
    hawaii.name = @"Hawaii";
    hawaii.capital = @"Honolulu";
    hawaii.motto = @"Ua Mau Ke Ea O Ka Aina I Ka Pono";
    hawaii.flag = [UIImage imageNamed:@"Flags/Small/hawaii-flag-small"];
    hawaii.population = @"1,431,603";
    hawaii.birdName = @"Hawaiian goose";
    hawaii.bird = [UIImage imageNamed:@"StateBirds/hawaiiBird.jpg"];
    
    StateInfo *idaho = [[StateInfo alloc]init];
    idaho.name = @"Idaho";
    idaho.capital = @"Boise";
    idaho.motto = @"Esto perpetua";
    idaho.flag = [UIImage imageNamed:@"Flags/Small/idaho-flag-small"];
    idaho.population = @"1,654,930";
    idaho.birdName = @"Mountain bluebird";
    idaho.bird = [UIImage imageNamed:@"StateBirds/idahoBird.jpg"];
    
    StateInfo *illinois = [[StateInfo alloc]init];
    illinois.name = @"Illinois";
    illinois.capital = @"Springfield";
    illinois.motto = @"State sovereignty, national union";
    illinois.flag = [UIImage imageNamed:@"Flags/Small/illinois-flag-small"];
    illinois.population = @"12,859,995";
    illinois.birdName = @"Northern cardinal";
    illinois.bird = [UIImage imageNamed:@"StateBirds/illinoisBird.jpg"];

    
    
    self.usStates = [NSMutableArray arrayWithObjects:alabama,alaska,arizona,arkansas, california, colorado, connecticut, florida, georgia, hawaii, idaho, illinois, nil];
    
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.usStates.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"stateInfoCellID";
    StatesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    
    
    //populate the cell
    StateInfo *item = [self.usStates objectAtIndex:indexPath.row];
    
    
    
    cell.stateName.text = item.name;
    cell.stateMotto.text = item.motto;
    cell.flagImageView.image = item.flag;
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    if ([[segue identifier] isEqualToString:@"showDetails"]) {
        StateDetailViewController *detailVC = [segue destinationViewController];
        NSIndexPath *myIndexPath = [self.tableView indexPathForSelectedRow];
        StateInfo *item = [self.usStates objectAtIndex:myIndexPath.row];
        detailVC.myState = item;
    }
    // Pass the selected object to the new view controller.
}


@end
