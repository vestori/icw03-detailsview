//
//  StateDetailViewController.h
//  sc00-US-States
//
//  Created by Orlando Gotera on 11/1/17.
//  Copyright © 2017 Orlando Gotera. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StateInfo.h"
@interface StateDetailViewController : UIViewController
@property (strong, nonatomic) StateInfo * myState;

@end
