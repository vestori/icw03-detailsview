//
//  StateInfo.h
//  sc00-US-States
//
//  Created by Orlando Gotera on 11/1/17.
//  Copyright © 2017 Orlando Gotera. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h> //needed for UIImage

@interface StateInfo : NSObject
@property(strong, nonatomic) NSString * name;
@property(strong, nonatomic) NSString * capital;
@property(strong, nonatomic) NSString * motto;
@property(strong, nonatomic) UIImage * flag;
@property(strong, nonatomic) NSString * birdName;
@property(strong, nonatomic) UIImage * bird;
@property(strong, nonatomic) NSString * population; 
@end
